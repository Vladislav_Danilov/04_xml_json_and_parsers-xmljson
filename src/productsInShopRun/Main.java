package productsInShopRun;

public class Main {

  public static void main(String[] args) {
    Converter converter = new Converter();
    // convert xml to json
    System.out.println("xml to json");
    System.out.println("");
    System.out.println(converter.objectToJson(converter.xmlToObject("resource/ProductInShopXML.xml")));
    System.out.println("");
    System.out.println("json to xml");
    System.out.println("");
    System.out.println(converter.objectToXml(converter.jsonToObject("resource/ProductInShopJSON.json")));
  }
}
