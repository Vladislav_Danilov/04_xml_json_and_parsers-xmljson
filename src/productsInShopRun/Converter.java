package productsInShopRun;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import productsInShopJAXB.Products;

/**
 * class contains methods to convert a string
 * 
 * @author Vladislav_Danilov
 *
 */
public class Converter {

  /**
   * method converts the object to xml
   * 
   * @param products object
   * @return string xml
   */
  public String objectToXml(Products products) {
    JAXBContext context;
    StringWriter writer = null;
    try {
      context = JAXBContext.newInstance(Products.class);
      Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
      writer = new StringWriter();
      marshaller.marshal(products, writer);
    } catch (JAXBException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    String xmlData = writer.toString();
    return xmlData;
  }

  /**
   * method converts the xml to object
   * 
   * @param patch xml file
   * @return object Products
   */
  public Products xmlToObject(String patch) {
    XMLInputFactory xif = XMLInputFactory.newFactory();
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    InputStream xml = classLoader.getResourceAsStream(patch);
    JAXBElement<Products> jb = null;
    XMLStreamReader xsr;
    try {
      xsr = xif.createXMLStreamReader(xml);
      xsr.nextTag();
      while (!xsr.getLocalName().equals("products")) {
        xsr.nextTag();
      }

      JAXBContext jc = JAXBContext.newInstance(Products.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();
      jb = unmarshaller.unmarshal(xsr, Products.class);
      xsr.close();
    } catch (XMLStreamException e) {
      e.printStackTrace();
    } catch (JAXBException e) {
      e.printStackTrace();
    }
    Products products = jb.getValue();
    return products;
  }

  /**
   * method converts the object to json
   * 
   * @param products
   * @return string json
   */
  public String objectToJson(Products products) {
    ObjectMapper mapper = new ObjectMapper();
    String jsonInString = null;
    try {
      mapper.enable(SerializationFeature.INDENT_OUTPUT);
      jsonInString = mapper.writeValueAsString(products);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
    return jsonInString;
  }

  /**
   * method converts the json to object
   * 
   * @param path
   * @return object Products
   */
  public Products jsonToObject(String path) {
    ObjectMapper mapper = new ObjectMapper();
    Products products = null;
    try {
      ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
      InputStream input = classLoader.getResourceAsStream(path);
      products = mapper.readValue(input, Products.class);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return products;
  }
}
